"use strict";
const student = {
    name: "",
    lastName: "",
    tabel: {},
};
let subject = prompt("Enter the name of subject: ");
let mark = +prompt("Enter the mark of subject: ");
while (subject) {
    student.tabel[subject] = mark;
    subject = prompt("Enter the name of subject: ");
    mark = +prompt("Enter the mark of subject: ");
};
let badMarks = 0;
let averageMark = 0;
for (const key in student.tabel) {
    averageMark += student.tabel[key];
    if (student.tabel[key] < 4) {
        badMarks++;
    }
};
if (!badMarks) {
    console.log("The student is transferred to the next course");
}
if (averageMark > 7) {
    console.log("Student assigned a scholarship");
}
console.log(`Number of bad marks: ${badMarks}`);
console.log(`Average student mark: ${averageMark}`);